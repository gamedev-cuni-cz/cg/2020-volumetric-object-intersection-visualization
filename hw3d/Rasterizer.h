#pragma once
#include "Bindable.h"
#include <array>

namespace Bind
{
	class Rasterizer : public Bindable
	{
	public:
		//Rasterizer( Graphics& gfx,bool twoSided );
		Rasterizer(Graphics& gfx, D3D11_CULL_MODE cullMode, D3D11_FILL_MODE fillMode);
		void Bind( Graphics& gfx ) noexcept override;
		//static std::shared_ptr<Rasterizer> Resolve( Graphics& gfx,bool twoSided );
		static std::shared_ptr<Rasterizer> Resolve(Graphics& gfx, D3D11_CULL_MODE cullMode, D3D11_FILL_MODE fillMode);
		static std::string GenerateUID(D3D11_CULL_MODE cullMode, D3D11_FILL_MODE fillMode);
		std::string GetUID() const noexcept override;
	protected:
		Microsoft::WRL::ComPtr<ID3D11RasterizerState> pRasterizer;
		bool twoSided;
	};
}
