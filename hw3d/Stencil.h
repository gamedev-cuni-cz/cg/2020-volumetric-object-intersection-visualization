#pragma once
#include "Bindable.h"
#include "BindableCodex.h"

namespace Bind
{
	class Stencil : public Bindable
	{
	public:
		enum class Mode
		{
			Off,
			Write,
			Mask,
			IntersectionWriteLastFrontFace,
			IntersectionCountBackFaces,
			IntersectionUseMask,
			CinderEdgesToDepthBuffer,
			CinderCountFaces,
			CinderMask
		};
		
		Stencil(Graphics& gfx, Mode mode) : mode(mode)
		{
			D3D11_DEPTH_STENCIL_DESC dsDesc = CD3D11_DEPTH_STENCIL_DESC{ CD3D11_DEFAULT{} };

			if (mode == Mode::Write)
			{
				dsDesc.DepthEnable = FALSE;
				dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

				dsDesc.StencilEnable = TRUE;
				dsDesc.StencilWriteMask = 0xFF;
				dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
				dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_INCR;
			}
			else if (mode == Mode::Mask)
			{
				dsDesc.DepthEnable = FALSE;
				dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;

				dsDesc.StencilEnable = TRUE;
				dsDesc.StencilReadMask = 0xFF;
				dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_LESS;
				dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			}
			else if (mode == Mode::IntersectionWriteLastFrontFace)
			{
				dsDesc.DepthEnable = TRUE;
				dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
				dsDesc.DepthFunc = D3D11_COMPARISON_GREATER;

				dsDesc.StencilEnable = TRUE;
				dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
				dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
			}
			else if (mode == Mode::IntersectionCountBackFaces)
			{
				dsDesc.DepthEnable = TRUE;
				dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
				dsDesc.DepthFunc = D3D11_COMPARISON_LESS;

				dsDesc.StencilEnable = TRUE;
				dsDesc.StencilWriteMask = 0xFF;
				dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
				dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_INCR;
			}
			else if (mode == Mode::IntersectionUseMask)
			{
				dsDesc.DepthEnable = FALSE;
				dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
				dsDesc.DepthFunc = D3D11_COMPARISON_ALWAYS;

				dsDesc.StencilEnable = TRUE;
				dsDesc.StencilReadMask = 0xFF;
				dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_EQUAL;
				dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
				dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_EQUAL;
				dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			}
			else if (mode == Mode::CinderEdgesToDepthBuffer)
			{
				dsDesc.DepthEnable = TRUE;
				dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ALL;
				dsDesc.DepthFunc = D3D11_COMPARISON_ALWAYS;

				dsDesc.StencilEnable = FALSE;
			}
			else if (mode == Mode::CinderCountFaces)
			{
				dsDesc.DepthEnable = TRUE;
				dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
				dsDesc.DepthFunc = D3D11_COMPARISON_LESS_EQUAL;

				dsDesc.StencilEnable = TRUE;
				dsDesc.StencilReadMask = 0xFF;
				dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
				dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_INCR;
				dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_ALWAYS;
				dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_DECR;
			}
			else if (mode == Mode::CinderMask)
			{
				dsDesc.DepthEnable = TRUE;
				dsDesc.DepthWriteMask = D3D11_DEPTH_WRITE_MASK_ZERO;
				dsDesc.DepthFunc = D3D11_COMPARISON_ALWAYS;

				dsDesc.StencilEnable = TRUE;
				dsDesc.StencilReadMask = 0xFF;
				dsDesc.FrontFace.StencilFunc = D3D11_COMPARISON_NOT_EQUAL;
				dsDesc.FrontFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
				dsDesc.BackFace.StencilFunc = D3D11_COMPARISON_NOT_EQUAL;
				dsDesc.BackFace.StencilPassOp = D3D11_STENCIL_OP_KEEP;
			}

			GetDevice(gfx)->CreateDepthStencilState(&dsDesc, &pStencil);
		}
		
		void Bind(Graphics& gfx) noexcept override
		{
			GetContext(gfx)->OMSetDepthStencilState(pStencil.Get(), 0);
		}
		
		static std::shared_ptr<Stencil> Resolve(Graphics& gfx, Mode mode)
		{
			return Codex::Resolve<Stencil>(gfx, mode);
		}
		
		static std::string GenerateUID(Mode mode)
		{
			using namespace std::string_literals;
			const auto modeName = [mode]() {
				switch (mode) {
				case Mode::Off:
					return "off"s;
				case Mode::Write:
					return "write"s;
				case Mode::Mask:
					return "mask"s;
				case Mode::IntersectionWriteLastFrontFace:
					return "intersectionFrontFaces"s;
				case Mode::IntersectionCountBackFaces:
					return "intersectionBackFaces"s;;
				case Mode::IntersectionUseMask:
					return "intersectionUseMask"s;
				case Mode::CinderEdgesToDepthBuffer:
					return "cinderEdgesToDepthBuffer"s;
				case Mode::CinderCountFaces:
					return "cinderCountFaces"s;
				case Mode::CinderMask:
					return "cinderMask"s;
				default:;
				}
				
				return "ERROR"s;
			};
			return typeid(Stencil).name() + "#"s + modeName();
		}
		
		std::string GetUID() const noexcept override
		{
			return GenerateUID(mode);
		}
		
	private:
		Mode mode;
		Microsoft::WRL::ComPtr<ID3D11DepthStencilState> pStencil;
	};
}
