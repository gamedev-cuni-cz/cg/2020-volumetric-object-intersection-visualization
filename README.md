# CGCG - Object Intersection
This project represents a semester project for the course "Computer Graphics for Game Development", taught at Charles University, Prague.
Author: Jaroslav Vozár


## Overall goal
*  Rendering system for intersections of objects using DirectX 11 made from scratch.

## Target result
* Versatile system for rendering intersections of objects and implementation into existing renderer.

## State of the art
* Several methods for rendering CSG data using stencil buffer.
* [A Z-Buffer CSG Rendering Algorithm for Convex Objects](https://dspace5.zcu.cz/bitstream/11025/15811/1/R41.pdf)
* [Improved CSG Rendering using Overlap Graph Subtraction Sequences](https://www.researchgate.net/publication/2559180_Improved_CSG_Rendering_using_Overlap_Graph_Subtraction_Sequences)

## Starting point:
* DirectX 11 template renderer (https://github.com/planetchili)

## Technologies, libraries and frameworks
* Visual Studio 2019
* DirectX 11
* Existing universal DirectX 11 renderer

## Final result
* Implementation of: [CInDeR - Collision and Interference Detection in Real-time](https://www.researchgate.net/publication/2875907_CInDeR_Collision_and_Interference_Detection_in_Real-time_using_Graphics_Hardware)